package com.ktyp.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle = new Circle(2);
        System.out.println(circle.toString());
        System.out.printf("%s area: %.3f \n", circle.getName(), circle.calArea());
        System.out.printf("%s perimeter: %.3f \n", circle.getName(), circle.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2.toString());
        System.out.printf("%s area: %.3f \n", circle.getName(), circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n", circle.getName(), circle2.calPerimeter());

        Triangle triangle = new Triangle(2, 2, 2);
        System.out.println(triangle.toString());
        System.out.printf("area: %.3f \n", triangle.calArea());
        System.out.printf("perimeter: %.3f \n", triangle.calPerimeter());
    }
}
